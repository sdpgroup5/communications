#include "SDPArduino.h"
#include <Wire.h>

// Distance unit is centimeters
// Angle unit is radian
// Time unit is seconds

#define MOTOR_LEFT 2
#define MOTOR_RIGHT 3
#define MOTOR_BACK 4
#define MOTOR_KICKER 5

#define MOTOR_MULT_LEFT -1
#define MOTOR_MULT_RIGHT -0.95
#define MOTOR_MULT_BACK 0.9

#define GRABBING 0
#define KICKING 1
#define IDLE 2

#define TRACE 1
#define DEBUG 2
#define INFO 3
#define WARN 4
#define ERROR 5
#define FATAL 6
#define OFF 7

#define LOG_LEVEL 7

#define STATUS_SUCCESS 0
#define STATUS_FAIL 1

#define COMMAND_ID_KICK 0
#define COMMAND_ID_GRAB 1
#define COMMAND_ID_STOP 2
#define COMMAND_ID_MOVE 3

// Number of data bytes
#define COMMAND_LENGTH_KICK 0
#define COMMAND_LENGTH_GRAB 0
#define COMMAND_LENGTH_STOP 0
#define COMMAND_LENGTH_MOVE 3

// Connection states
#define WAIT_SYN 0
#define RUNNING 1

// Acknowledgement flag
#define ACK_BIT 0x10
// Synchronize flag
#define SYN_BIT 0x08
// Enforce flag
#define ENF_BIT 0x04

// #define USE_CRC

#define MAX_PACKET_BYTES 5

#define MAX_FRAME_BYTES MAX_PACKET_BYTES + 1

// TODO 25?
#define MIN_POWER 25


// Power / pulse states
#define PULSE_PERIOD 400
#define PULSE_COOLDOWN 200
#define TIME_PER_POWER 4
#define MIN_PULSE_WIDTH 100

#define HEARTBEAT_ID 0

#define MAX_DOWNTIME 100

typedef struct {
  int power;
  int pulse_start;
  int pulse_width;
} motor_state;

void set_movement(motor_state* state, int power);
void handle_movement(motor_state* state, int motor_num, float motor_mult);

unsigned char bidirectional_lookup[64] = {
  0x59, // Encode 000000 -> 01011001
  0x71, // Encode 000001 -> 01110001
  0x72, // Encode 000010 -> 01110010
  0xff,
  0x65, // Encode 000100 -> 01100101
  0xff,
  0xff,
  0xff,
  0x69, // Encode 001000 -> 01101001
  0xff,
  0xff,
  0x0f, // Decode 001111 <- 00001011
  0xff,
  0x3d, // Decode 111101 <- 00001101
  0x3e, // Decode 111110 <- 00001110
  0x4b, // Encode 001111 -> 01001011
  0x53, // Encode 010000 -> 01010011
  0xff,
  0xff,
  0x10, // Decode 010000 <- 00010011
  0xff,
  0xff,
  0x37, // Decode 110111 <- 00010110
  0xff,
  0xff,
  0x00, // Decode 000000 <- 00011001
  0x3b, // Decode 111011 <- 00011010
  0xff,
  0x1f, // Decode 011111 <- 00011100
  0xff,
  0xff,
  0x5c, // Encode 011111 -> 01011100
  0x63, // Encode 100000 -> 01100011
  0xff,
  0xff,
  0x20, // Decode 100000 <- 00100011
  0xff,
  0x04, // Decode 000100 <- 00100101
  0x3f, // Decode 111111 <- 00100110
  0xff,
  0xff,
  0x08, // Decode 001000 <- 00101001
  0xff,
  0xff,
  0x2f, // Decode 101111 <- 00101100
  0xff,
  0xff,
  0x6c, // Encode 101111 -> 01101100
  0x74, // Encode 110000 -> 01110100
  0x01, // Decode 000001 <- 00110001
  0x02, // Decode 000010 <- 00110010
  0xff,
  0x30, // Decode 110000 <- 00110100
  0xff,
  0xff,
  0x56, // Encode 110111 -> 01010110
  0xff,
  0xff,
  0xff,
  0x5a, // Encode 111011 -> 01011010
  0xff,
  0x4d, // Encode 111101 -> 01001101
  0x4e, // Encode 111110 -> 01001110
  0x66  // Encode 111111 -> 01100110
};

// --- Control characters ---
// Frame delimiter
unsigned char frame_end = 0x47;

// Unused
unsigned char control_1 = 0x55;
unsigned char control_3 = 0x78;
unsigned char control_4 = 0x6a;

// --- Arduino Logic Stuff ---
unsigned char frame_buffer[MAX_FRAME_BYTES];
int frame_buffer_pos = 0;

int connection_state = WAIT_SYN;
int expected_sequence_number = 0;

unsigned long kicker_timestamp = 0;
int kicker_state = IDLE;

motor_state state_left = {0, 0, 0};
motor_state state_back = {0, 0, 0};
motor_state state_right = {0, 0, 0};

unsigned long hb_timestamp = 0;

unsigned long command_timestamp = 0;

// int power_history[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

int popcount(unsigned char c) {
  int i;
  int count = 0;
  for (i = 0; i < 8; i++) {
    if ((c >> i) & 0x1) {
      count++;
    }
  }
  return count;
}

void encode_char(unsigned char raw, unsigned char *encoded) {
  unsigned char byte_mask = 0x3f;
  unsigned char masked = raw & byte_mask;
  int disparity = 2 * popcount(masked) - 6;

  if (disparity == 0) {
    *encoded = 0x80 | masked;
  } else if (disparity == 2 && masked != 0xf) {
    *encoded = masked;
  } else if (disparity == -2 && masked != 0x30) {
    *encoded = 0xc0 | masked;
  } else {
    *encoded = bidirectional_lookup[masked];
  }
}

int decode_char(unsigned char raw, unsigned char *decoded) {
  unsigned char prefix_mask = 0xc0;
  unsigned char byte_mask = 0x3f;
  unsigned char prefix = raw & prefix_mask;
  unsigned char masked = raw & byte_mask;
  int disparity = 2 * popcount(raw) - 8;

  // Illegal codeword
  if (disparity != 0) {
    return STATUS_FAIL;
  }

  // Disparity 0
  if (prefix == 0x80) {
    *decoded = masked;
    return 2;
  }
  // Disparity 2
  else if (prefix == 0x0) {
    // Illegal codeword
    if (masked == 0xf) {
      return STATUS_FAIL;
    }
    *decoded = masked;
    return 3;
  }
  // Disparity -2
  else if (prefix == 0xc0) {
    // Illegal codeword
    if (masked == 0x30) {
      return STATUS_FAIL;
    }
    *decoded = masked;
    return 4;
  }
  // Other disparities
  else {
    *decoded = bidirectional_lookup[masked];
    return 5;
  }

  return STATUS_SUCCESS;
}

// Encode packet -> frame
void encode(unsigned char *packet, unsigned char *frame, int length) {
  int i;
  for (i = 0; i < length; i++) {
    encode_char(packet[i], frame + i);
  }
}

// Decode frame -> packet
int decode(unsigned char *frame, unsigned char *packet, int length) {
  int i;
  for (i = 0; i < length; i++) {
    if (decode_char(frame[i], packet + i) == STATUS_FAIL) {
      return STATUS_FAIL;
    }
  }
  return STATUS_SUCCESS;
}

// Serialize [id, seq_no] -> packet
void serialize(unsigned char *packet, int sequence_number, int acknowledge,
  int synchronize, int command_id) {

  packet[0] = (sequence_number << 5) |
              (acknowledge ? ACK_BIT : 0) |
              (synchronize ? SYN_BIT : 0) |
              (command_id & 0x7);

  #ifdef USE_CRC
  // TODO CRCs
  #else
    packet[1] = 0;
  #endif
}

// Deserialize [id, seq_no, data] <- packet
void deserialize(unsigned char *packet, int packet_length,
  int *sequence_number, int *acknowledge, int *synchronize,
  int *command_id, unsigned char *data) {

  // TODO CRCs

  *sequence_number = packet[0] >> 5;
  *acknowledge = (packet[0] & ACK_BIT) != 0;
  *synchronize = (packet[0] & SYN_BIT) != 0;
  *command_id = packet[0] & 0x7;

  int i;
  for (i = 2; i < packet_length; i++) {
    data[i - 2] = packet[i];
  }
}

void setup() {
  Serial.flush();
  pinMode(13, OUTPUT);
  pinMode(8, OUTPUT);    // initialize pin 8 to control the radio
  digitalWrite(8, HIGH); // select the radio
  Serial.begin(9600);
  SDPsetup();
}

void loop() {
  while(Serial.available() > 0) {
    int in_char = Serial.read();
    if (in_char != -1) {
      frame_buffer[frame_buffer_pos] = in_char;
      if ((unsigned char) in_char == frame_end) {
        if (frame_buffer_pos < 2) {
          // Must have at least 2 header bytes and a frame_end to be valid
          frame_buffer_pos = 0;
          break;
        }

        // Decode
        unsigned char packet[frame_buffer_pos];
        int status = decode(frame_buffer, packet, frame_buffer_pos);

        // Deserialize
        int sequence_number;
        int acknowledge;
        int synchronize;
        int command_id;

        int packet_content_length = frame_buffer_pos;
        unsigned char data[packet_content_length - 1];
        deserialize(packet, packet_content_length, &sequence_number,
          &acknowledge, &synchronize, &command_id, data);
        if (synchronize) {
          if (acknowledge) {
            // SYN-ACK: move to running state
            connection_state = RUNNING;
          } else {
            // SYN: reset expected sequence_number to 0 and send SYN-ACK
            expected_sequence_number = 0;

            // Serialize SYN-ACK
            unsigned char ack_packet[2];
            serialize(ack_packet, 0, 1, 1, 0);

            // Encode SYN-ACK
            int ack_frame_length = 3;
            unsigned char ack_frame[ack_frame_length];
            encode(ack_packet, ack_frame, 2);
            ack_frame[ack_frame_length - 1] = frame_end;
            Serial.write(ack_frame, ack_frame_length);
          }
        } else if (connection_state == RUNNING) {
          if (((command_id == COMMAND_ID_STOP || command_id == COMMAND_ID_GRAB
              || command_id == COMMAND_ID_KICK) && packet_content_length - 2 == 0)
              || (command_id == COMMAND_ID_MOVE && packet_content_length - 2 == 3)) {
            // If valid packet, do things
            if (sequence_number == expected_sequence_number) {
              // Command: execute command and update seq. no.
              expected_sequence_number = 1 - expected_sequence_number;
              execute_command(command_id, data, packet_content_length - 2);
            }

            // Always ACK expected sequence number
            // Serialize ACK
            unsigned char ack_packet[2];
            serialize(ack_packet, expected_sequence_number, 1, 0, command_id);

            // Encode ACK
            int ack_frame_length = 3;
            unsigned char ack_frame[ack_frame_length];
            encode(ack_packet, ack_frame, 2);
            ack_frame[ack_frame_length - 1] = frame_end;
            Serial.write(ack_frame, ack_frame_length);

          }
        }
        frame_buffer_pos = 0;
      } else if (frame_buffer_pos < MAX_FRAME_BYTES - 1) {
        frame_buffer_pos++;
      }
    }
  }

  if (connection_state == WAIT_SYN) {
    // Serialize SYN
    unsigned char ack_packet[2];
    serialize(ack_packet, 0, 0, 1, 0);

    // Encode SYN
    int ack_frame_length = 3;
    unsigned char ack_frame[ack_frame_length];
    encode(ack_packet, ack_frame, 2);
    ack_frame[ack_frame_length - 1] = frame_end;
    Serial.write(ack_frame, ack_frame_length);

  } else if (hb_timestamp + 100 < millis()) {
    // Serialize Heartbeat
    unsigned char hb_packet[2];
    serialize(hb_packet, 0, 0, 0, HEARTBEAT_ID);

    // Encode Heartbeat
    int hb_frame_length = 3;
    unsigned char hb_frame[hb_frame_length];
    encode(hb_packet, hb_frame, 2);
    hb_frame[hb_frame_length - 1] = frame_end;
    Serial.write(hb_frame, hb_frame_length);

    hb_timestamp = millis();
  }

  // If we haven't received commands for a while, stop
  if (millis() - command_timestamp > MAX_DOWNTIME) {
    if (abs((&state_left)->power) > MIN_POWER) {
      set_movement(&state_left, 0);
    }
    if (abs((&state_back)->power) > MIN_POWER) {
      set_movement(&state_back, 0);
    }
    if (abs((&state_right)->power) > MIN_POWER) {
      set_movement(&state_right, 0);
    }
  }

  handle_kicker();
  handle_movement(&state_left, MOTOR_LEFT, MOTOR_MULT_LEFT);
  handle_movement(&state_back, MOTOR_BACK, MOTOR_MULT_BACK);
  handle_movement(&state_right, MOTOR_RIGHT, MOTOR_MULT_RIGHT);

  Serial.flush();
}

void execute_command(int command_id, unsigned char *data, int data_length) {
  switch (command_id) {
    case COMMAND_ID_KICK:
      if (data_length == COMMAND_LENGTH_KICK) {
        set_kick();
        command_timestamp = millis();
      }
      break;
    case COMMAND_ID_GRAB:
      if (data_length == COMMAND_LENGTH_GRAB) {
        set_grab();
        command_timestamp = millis();
      }
      break;
    case COMMAND_ID_MOVE:
      if (data_length == COMMAND_LENGTH_MOVE) {
        set_movement(&state_left, decode_motor_val(data[0]));
        set_movement(&state_back, decode_motor_val(data[1]));
        set_movement(&state_right, decode_motor_val(data[2]));
        command_timestamp = millis();
      }
      break;
    case COMMAND_ID_STOP:
      if (data_length == COMMAND_LENGTH_STOP) {
        set_movement(&state_left, 0);
        set_movement(&state_back, 0);
        set_movement(&state_right, 0);
        command_timestamp = millis();
      }
      break;
    default:
      break;
  }
}

int decode_motor_val(float src) {
  int raw = (int) ((src - 30.0) * (100.0 / 30.0));
  return clamp(raw, -100, 100);
}

void set_grab() {
  kicker_timestamp = millis();
  kicker_state = GRABBING;
}

void set_kick() {
  kicker_timestamp = millis();
  kicker_state = KICKING;
}

void set_movement(motor_state* state, int power) {
  unsigned long cur_time = millis();
  if (abs(power) > MIN_POWER || power == 0) {
    state->power = power;
    // Reset pulse start
    state->pulse_start = 0;
    // If in continuous mode, pulse_width is irrelevant
  } else if (abs(power) > 0 && cur_time - state->pulse_start > PULSE_PERIOD) {
    state->power = power > 0 ? MIN_POWER : -MIN_POWER;
    state->pulse_start = cur_time - (cur_time % PULSE_PERIOD);
    state->pulse_width = (abs(power) * TIME_PER_POWER) + MIN_PULSE_WIDTH;
  }
}

// Set kicker power according to latest command
void handle_kicker() {
  unsigned long delta_time = millis() - kicker_timestamp;
  switch (kicker_state) {
    case GRABBING:
      if (delta_time <= 500) {
        motorForward(MOTOR_KICKER, 100);
        log(TRACE, "performing grab.");
      } else if (delta_time <= 800) {
        motorForward(MOTOR_KICKER, 40);
        log(TRACE, "de-loading grab.");
      } else {
        kicker_state = IDLE;
        motorStop(MOTOR_KICKER);
        log(TRACE, "done grabbing.");
      }
      break;
    case KICKING:
      if (delta_time <= 300) {
        motorBackward(MOTOR_KICKER, 100);
        log(TRACE, "performing kick.");
      } else if (delta_time <= 480) {
        motorBackward(MOTOR_KICKER, 40);
        log(TRACE, "de-loading kick.");
      } else {
        kicker_state = IDLE;
        motorStop(MOTOR_KICKER);
        log(TRACE, "done kicking.");
      }
      break;
    case IDLE:
      break;
  }
}

// Set wheel motor powers according to latest commands
void handle_movement(motor_state* state, int motor_num, float motor_mult) {
  unsigned long cur_time = millis();

  int power;
  if (abs(state->power) > MIN_POWER || state->power == 0) {
    power = state->power;
  } else if ((cur_time - state->pulse_start) % PULSE_PERIOD < state->pulse_width) {
    if ((cur_time - state->pulse_start) % PULSE_PERIOD < 125) {
      power = state->power > 0 ? 60 : -60;
    } else {
      power = state->power > 0 ? 30 : -30;
    }
  } else {
    power = 0;
  }

  power *= motor_mult;

  // Set motors (un-negative the backwards values)
  if (power < 0) {
    motorBackward(motor_num, -power);
  } else {
    motorForward(motor_num, power);
  }
}

// Clamps an int to the specified range: [min, max]
int clamp(int value, int min, int max) {
  // Error handling?
  if (min > max) {
    return value;
  }
  if (value < min) {
    return min;
  }
  if (value > max) {
    return max;
  }
  return value;
}

void log(int level, char *message) {
  if (level < LOG_LEVEL) {
    return;
  }

  // Serial.print("LOG ");
  // Serial.print(level);
  // Serial.print(" ");
  // Serial.println(message);
  // Serial.flush();
}
