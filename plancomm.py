#!/usr/bin/env python

'''Implement communication between the planners'''

__author__ = "Boris Penev"

import socket
import multiprocessing
import time

# log levels: "TRACE", "DEBUG", "INFO", "WARN", "ERROR", "FATAL", "OFF"
# off is 0, fatal is 1, error 2, warn 3, info 4, debug 5, trace 6
LOG_PLAN = 2

if LOG_PLAN > 0:
    import sys

# stdout logging lock
if LOG_PLAN > 0:
    IOLOCK = multiprocessing.Lock()

# timeout for socket reconnection
socket.setdefaulttimeout(3.0)

class _PlannerCommunications(multiprocessing.Process):
    '''
    Communications between the planners
    '''
    def __init__(self, host, buffer_size = 4096, daemon = True):
        super(_PlannerCommunications, self).__init__()
        #self.daemon = True
        self.host   = host
        self.buffsz = buffer_size
        self.daemon = daemon
        self.lock   = multiprocessing.Lock()
        self._data  = multiprocessing.Array('c', 32)

    def run(self):
        self.create_socket()

    def create_socket(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        while True:
            try:
                self.socket.bind(self.host)
                break
            except socket.error:
                if LOG_PLAN > 1:
                    with IOLOCK:
                        print 'Socket error exception when binding port'
                        print sys.exc_info()
                time.sleep(2)
                continue
            except SystemExit:
                # Clean up (shutdown socket)
                self.__del__()
                raise
            except KeyboardInterrupt:
                # Clean up (shutdown socket)
                self.__del__()
                raise
            except:
                if LOG_PLAN > 1:
                    with IOLOCK:
                        print 'Unexpected error at socket bind:', sys.exc_info()
                raise


    def recreate_socket(self):
        self.destroy_socket()
        self.create_socket()

    def destroy_socket(self):
        try:
            self.socket.shutdown(socket.SHUT_RDWR)
        except:
            pass
        try:
            self.socket.close()
        except:
            pass

    def __del__(self):
        self.destroy_socket()
        # multiprocessing.Process has no method __del__
        #super(_PlannerCommunications, self).__del__()

class PlannerSender(_PlannerCommunications):
    '''
    Output communication to the allied planner.
    Start it with .start().
    '''
    def __init__(self, host, client, buffer_size = 4096, daemon = True):
        super(PlannerSender, self).__init__(host = host,
                buffer_size = buffer_size, daemon = daemon)
        self.client = client
        self.conn = None

    def create_socket(self):
        super(PlannerSender, self).create_socket()
        self.socket.listen(1) # max 5

    def recreate_socket(self):
        try:
            self.conn.close()
        except:
            pass
        super(PlannerSender, self).recreate_socket()
        self.socket.listen(1) # max 5
        self.conn = None

    def __del__(self):
        try:
            if self.conn is not None:
                self.conn.close()
        except:
            pass
        super(PlannerSender, self).__del__()

    @property
    def data(self):
        with self.lock:
            return self._data.value

    @data.setter
    def data(self, data):
        with self.lock:
            #self._data = data
            self._data.value = data

    def run(self):
        self.create_socket()
        recreate = False
        while True:
            try:
                # 25 fps, hence 40 ms between frames
                time.sleep(0.04)
                if LOG_PLAN > 5:
                    with IOLOCK:
                        print 'Enter while loop at sender'
                if self.conn is None:
                    if LOG_PLAN > 4:
                        with IOLOCK:
                            print 'Connection is None at sender'
                    self.conn, addr = self.socket.accept()
                    if addr != self.client:
                        if LOG_PLAN > 2:
                            with IOLOCK:
                                print 'address is bad at sender'
                        self.conn.close()
                        self.conn = None
                        continue
                if recreate: # recreate socket
                    recreate = False
                    if LOG_PLAN > 3:
                        with IOLOCK:
                            print 'Recreate at sender'
                    self.recreate_socket()
                    continue
                #if random.random() < 0.6:
                    #self.conn.close()
                    #self.conn = None
                    #continue
                self._send()
            except socket.timeout:
                if self.conn is not None:
                    recreate = True
                if LOG_PLAN > 2:
                    with IOLOCK:
                        print 'Timeout exception at sender'
            except socket.error:
                if self.conn is not None:
                    recreate = True
                if LOG_PLAN > 2:
                    with IOLOCK:
                        print 'Socket error exception at sender'
                        #print sys.exc_info()
            except SystemExit:
                # Clean up (shutdown socket)
                self.__del__()
                raise
            except KeyboardInterrupt:
                # Clean up (shutdown socket)
                self.__del__()
                raise
            except:
                if LOG_PLAN > 1:
                    with IOLOCK:
                        print 'Unexpected error at sender:', sys.exc_info()
                raise

    def _send (self):
            #self.start()
            if LOG_PLAN > 5:
                with IOLOCK:
                    print 'sender'
                    #print 'sender data:', self.data
            if LOG_PLAN > 4:
                with IOLOCK:
                    print 'sending data:', self.data
            self.conn.send(self.data)
            #self.conn.close()
            if LOG_PLAN > 3:
                with IOLOCK:
                    print 'sent     data:', self.data


class PlannerReceiver(_PlannerCommunications):
    '''
    Input communication from the allied planner
    Start it with .start().
    '''
    def __init__(self, host, server, buffer_size = 4096, daemon = True):
        super(PlannerReceiver, self).__init__(host = host,
                buffer_size = buffer_size, daemon = daemon)
        self.server = server

    def create_socket(self):
        super(PlannerReceiver, self).create_socket()

    def __del__(self):
        super(PlannerReceiver, self).__del__()

    @property
    def data(self):
        with self.lock:
            return self._data.value

    def run(self):
        self.create_socket()
        connected = False
        recreate = False
        while True:
            try:
                # 25 fps, hence 40 ms between frames
                time.sleep(0.04)
                if not connected:
                    if LOG_PLAN > 2:
                        with IOLOCK:
                            print 'not connected receiver'
                    self.socket.connect(self.server)
                    if self.socket.getpeername() == self.server:
                        if LOG_PLAN > 3:
                            with IOLOCK:
                                print 'connection is good at receiver'
                        connected = True
                    continue
                if recreate:
                    recreate = False
                    if LOG_PLAN > 3:
                        with IOLOCK:
                            print 'Recreate at receiver'
                    self.recreate_socket()
                    connected = False
                    continue
                #if random.random() < 0.6:
                    #recreate = True
                    #continue
                self._receive()
                if self.data == '':
                    self.recreate_socket()
                    connected = False
            except socket.timeout:
                if connected:
                    recreate = True
                if LOG_PLAN > 2:
                    with IOLOCK:
                        print 'Timeout exception at receiver'
            except socket.error:
                if connected:
                    recreate = True
                if LOG_PLAN > 2:
                    with IOLOCK:
                        print 'Socket error exception at receiver'
                        #print sys.exc_info()
            except SystemExit:
                # Clean up (shutdown socket)
                self.__del__()
                raise
            except KeyboardInterrupt:
                # Clean up (shutdown socket)
                self.__del__()
                raise
            except:
                if LOG_PLAN > 1:
                    with IOLOCK:
                        print 'Unexpected error at receiver:', sys.exc_info()
                raise

    def _receive(self):
            if LOG_PLAN > 5:
                with IOLOCK:
                    print 'receiver'
                    #print 'receiver data:', self.data
            if LOG_PLAN > 4:
                with IOLOCK:
                    print 'receiving data:'
            data = self.socket.recv(self.buffsz)
            if LOG_PLAN > 3:
                with IOLOCK:
                    print 'received data:', data
            if not data or len(data) == 0:
              raise socket.error
            if data and len(data) > 0 and len(data) < 32:
                self._data.value = data
            #self.socket.close()

def _local_loopback():
    sender1 = PlannerSender( host   = ('127.0.0.1', 5001),
                             client = ('127.0.0.1', 5003))
    sender2 = PlannerSender( host   = ('127.0.0.1', 5002),
                             client = ('127.0.0.1', 5004))
    recvr1 = PlannerReceiver(host   = ('127.0.0.1', 5003),
                             server = ('127.0.0.1', 5001))
    recvr2 = PlannerReceiver(host   = ('127.0.0.1', 5004),
                             server = ('127.0.0.1', 5002))
    sender1.start()
    sender2.start()
    recvr1.start()
    recvr2.start()
    count1 = 0
    count2 = 1 << 10
    while True:
        # 25 fps, hence 40 ms between frames
        time.sleep(0.04)
        sender1.data = str(count1)
        sender2.data = str(count2)
        count1 += 1
        count2 -= 1
        count1 %= 1 << 10
        count2 %= 1 << 10
        print recvr1.data, '\t\t', recvr2.data

def _host_1(local_ip, remote_ip, lcl_snd_port, lcl_rcv_port,
           rmt_snd_port, rmt_rcv_port):
    sender1 = PlannerSender( host   = (local_ip,  lcl_snd_port),
                             client = (remote_ip, rmt_rcv_port))
    recvr1 = PlannerReceiver(host   = (local_ip,  lcl_rcv_port),
                             server = (remote_ip, rmt_snd_port))
    sender1.start()
    recvr1.start()
    count1 = 0
    while True:
        # 25 fps, hence 40 ms between frames
        time.sleep(0.04)
        sender1.data = str(count1)
        count1 += 1
        count1 %= 1 << 10
        print recvr1.data

def _host_2(local_ip, remote_ip, lcl_snd_port, lcl_rcv_port,
           rmt_snd_port, rmt_rcv_port):
    sender1 = PlannerSender( host   = (local_ip,  lcl_snd_port),
                             client = (remote_ip, rmt_rcv_port))
    recvr1 = PlannerReceiver(host   = (local_ip,  lcl_rcv_port),
                             server = (remote_ip, rmt_snd_port))
    sender1.start()
    recvr1.start()
    count2 = 1 << 10
    while True:
        # 25 fps, hence 40 ms between frames
        time.sleep(0.04)
        sender1.data = str(count2)
        count2 -= 1
        count2 %= 1 << 10
        print recvr1.data
