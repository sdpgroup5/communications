'''Arduino commands'''

__author__ = 'Sebastian'

from vector2 import *
from datetime import datetime

class _BasicCommand(object):

    wheels = ['left', 'back', 'right']
    wheel_properties = {
        'left': {
            'angle': math.pi, # Angle of wheel w.r.t. robot
            'half_track': 7.4 # Distance from center to wheel (cm)
        },
        'back': {
            'angle': 3.0/2 * math.pi,
            'half_track': 7.4
        },
        'right': {
            'angle': 0,
            'half_track': 7.4
        }
    }

    def __init__(self, priority=0, log=True):
        if priority < 0:
            raise ValueError('priority must be >= 0')

        self.priority = priority
        self.log = log
        self.timestamp = datetime.now()

    def serialize_type(self):
        return 0

    def serialize_data(self):
        return []

class KickCommand(_BasicCommand):

    def __init__(self):
        super(KickCommand, self).__init__(priority=2)

    def serialize_type(self):
        return 0

    def __repr__(self):
        return 'Kick'

class GrabCommand(_BasicCommand):

    def __init__(self):
        super(GrabCommand, self).__init__(priority=2)

    def serialize_type(self):
        return 1

    def __repr__(self):
        return 'Grab'

class StopCommand(_BasicCommand):

    def __init__(self):
        super(StopCommand, self).__init__(priority=0, log=False)

    def serialize_type(self):
        return 2

    def __repr__(self):
        return 'Stop'

class MoveCommand(_BasicCommand):

    def __init__(self, power=None, angle=0, rotation_power=0):
        super(MoveCommand, self).__init__(priority=1, log=True)
        self.power = Vector2(0, 0) if power is None else power
        self.angle = angle
        self.rotation_power = rotation_power

    def serialize_type(self):
        return 3

    def serialize_data(self):
        """ Make state values into appropriate control
        statement string for sending to the arduino

        Returns:
            String with control statement.
        """

        # Convert high level -> wheel powers
        wheel_powers = {}
        for wheel, prop in self.wheel_properties.iteritems():
            # Distance of wheel from robot center
            half_track = prop['half_track']

            # Angle of robot wrt wheel
            a_robot_wrt_wheel = -prop['angle']

            # Angle of translation power wrt wheel
            a_pow_wrt_wheel = a_robot_wrt_wheel \
                + self.power.angle() - self.angle

            # Translation power + Rotation power (wrt wheel)
            wheel_powers[wheel] = math.cos(a_pow_wrt_wheel) \
                * self.power.len() + self.rotation_power

        for wheel, power in wheel_powers.iteritems():
            # Encode [-1, 0] -> [0,30] and  [-1, 0] -> [30, 60]
            power = int(power * 30.0) + 30

            # Clamp to [0, 60]
            wheel_powers[wheel] = clamp(power, 0, 60)

        # Build encoding
        encoding = []
        for wheel in self.wheels:
            encoding.append(wheel_powers[wheel])
        return encoding

    def __repr__(self):
        return 'Move [pow=%s, ang=%+.3f, rot=%+.3f]' \
            % (str(self.power), self.angle, self.rotation_power)

# Clamping / limiting utility
def clamp(value, min, max):
    """ Restrict values to within a specified range.

    Args:
        value: you know, the value.
        min: the minimum value
        max: I'll leave this to you

    Returns:
        A value.

    Raises:
        ValueError: If the minimum is bigger than the maximum
    """
    if min > max:
        raise ValueError
    if value < min:
        return min
    if value > max:
        return max
    return value

# Container for packet data
class CommandPacket(object):
    def __init__(self, header, data):
        self.header = header
        self.data = data

class SequenceNumberException(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

class CommandNumberException(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

class DataSizeException(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)