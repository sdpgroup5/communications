'''Implements the physical layer.
Specifically, the physical coding sublayer.'''

import serial
import threading
from serialpacket import *

_6b_8b_encoding_lookup = {
    # Disparity -6
    0x00: 0x59,

    # Disparity +6
    0x3f: 0x66,

    # Disparity -4
    0x01: 0x71,
    0x02: 0x72,
    0x04: 0x65,
    0x08: 0x69,
    0x10: 0x53,
    0x20: 0x63,

    # Disparity +4
    0x3e: 0x4e,
    0x3d: 0x4d,
    0x3b: 0x5a,
    0x37: 0x56,
    0x2f: 0x6c,
    0x1f: 0x5c,

    # Disparity -2
    0x30: 0x74,

    # Disparity +2
    0x0f: 0x4b
}

_6b_8b_decoding_lookup = dict([[v, k] for [k, v] in \
    _6b_8b_encoding_lookup.iteritems()])

# --- Control characters ---
# Frame delimiter
_frame_end = 0x47

# Unused
_control_2 = 0x55
_control_3 = 0x78
_control_4 = 0x6a

class SerialSocket(object):

    def __init__(self, port, baud=9600, timeout=0.1):
        # Initialize without port to defer opening until first enabling
        self._serial = serial.Serial(baudrate=baud, timeout=timeout)
        self._serial.port = port

    def open(self):
        self._serial.open()

    def close(self):
        self._serial.flush()
        self._serial.close()

    def send(self, packet):
        payload = []
        payload.extend(packet.header)
        payload.extend(packet.data)

        frame = self._encode_6b_8b(payload)
        frame.append(_frame_end)

        self._serial.write(str(bytearray(frame)))

    def receive(self):
        # 1 byte header, n bytes data, 1 byte frame_end
        frame = self.readline(eol=chr(_frame_end))

        if len(frame) < 3:
            raise TimeoutException('frames must be at least 3 bytes long')

        payload = self._decode_6b_8b(frame[:-1])

        return SerialPacket.from_payload(payload)

    def readline(self, size=None, eol='\n'):
        """read a line which is terminated with end-of-line (eol) character
        ('\n' by default) or until timeout."""
        leneol = len(eol)
        line = bytearray()
        while True:
            c = self._serial.read(1)
            if c:
                # print "0x%0.2x" % ord(c)
                line += c
                if line[-leneol:] == eol:
                    break
                if size is not None and len(line) >= size:
                    break
            else:
                raise TimeoutException('No eol sequence detected')
        return bytes(line)

    # Line Coding
    def _encode_6b_8b(self, data):
        """Encode data with a DC-balanced 6b/8b coding scheme"""
        code = []
        for byte in data:
            byte_mask = 0x3f
            byte = byte & byte_mask
            disparity = 2 * bin(byte).count('1') - 6
            if disparity == 0:
                codeword = 0x80 | byte
            elif disparity == 2 and byte != 0xf:
                codeword = byte
            elif disparity == -2 and byte != 0x30:
                codeword = 0xc0 | byte
            else:
                codeword = _6b_8b_encoding_lookup[byte]
            code.append(codeword)
        return code

    def _decode_6b_8b(self, code):
        """Encode data with a DC-balanced 6b/8b coding scheme"""
        data = []
        for char in code:
            codeword = ord(char)
            # print "code: " + bin(codeword)[2:].zfill(8) + "\r"
            disparity = 2 * bin(codeword).count('1') - 8
            # Illegal codeword
            if (disparity != 0):
                raise DecodeException('Invalid codeword')

            prefix_mask = 0xc0
            byte_mask = 0x3f
            prefix = codeword & prefix_mask
            if prefix == 0x80:
                byte = codeword & byte_mask
            elif prefix == 0x0:
                # Illegal codeword
                if codeword & byte_mask == 0xf:
                    raise DecodeException('Invalid codeword')
                byte = codeword & byte_mask
            elif prefix == 0xc0:
                # Illegal codeword
                if codeword & byte_mask == 0x30:
                    raise DecodeException('Invalid codeword')
                byte = codeword & byte_mask
            else:
                byte = _6b_8b_decoding_lookup[codeword]
            data.append(byte)
            # print "real: " + bin(byte)[2:].zfill(8) + "\r"
        return data

class TimeoutException(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

class DecodeException(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)
