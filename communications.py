'''Implements the transport layer.'''

__author__ = 'Allan, Sebastian'

import serial
import time
import threading
from datetime import datetime, timedelta
from copy import copy
import sys
from command import *
from serialsocket import *
from serialpacket import *

LOG_LEVELS = ["TRACE", "DEBUG", "INFO", "WARN", "ERROR", "FATAL", "OFF"]
LOG_COMM = False

class States(object):
    wait_syn = 'wait_syn'
    running = 'running'

class Communications(object):
    """ Python interface to control the robot over serial.

    Should be kept in sync with arduino firmware or strange things might occur.
    """

    def __init__(self, port, baud=9600, timeout=0.03, enabled=True):
        self._socket = SerialSocket(port=port, baud=baud, timeout=timeout)
        self._socket.open()

        self._state = States.wait_syn
        self.state_change_callback = None

        # Main set of commands
        self._commands = {}

        # Frozen commands for when comms gets disabled and we need to ensure
        # all existing commands get flushed, but we don't want to block forever
        self._frozen_commands = {}

        # Lock for all interactions with command dicts. Frozen or otherwise
        self._commands_lock = threading.RLock()

        # Event used to signal when commands are available
        self._commands_available = False

        self._enabled = threading.Event()
        self.enabled = enabled

        # TODO replace with multiprocessing.Process()
        self._stopAndWaitThread = StopAndWaitThread(self)
        self._stopAndWaitThread.start()

    @property
    def enabled(self):
        return self._enabled.isSet()

    @enabled.setter
    def enabled(self, value):
        if value:
            # Lock commands
            self._commands_lock.acquire()

            # Unfreeze
            for command_type, command in self._frozen_commands.iteritems():
                if not command_type in self._commands:
                    self._commands[command_type] = command

            self._frozen_commands.clear()

            # If there are commands available, set flag.
            if len(self._commands) > 0:
                self._commands_available = True

            # Toggle enabled on
            self._enabled.set()

            # Unlock commands
            self._commands_lock.release()
        else:
            # Lock commands
            self._commands_lock.acquire()

            # TODO maybe not?
            self.put_command(StopCommand())

            # Freeze
            for command_type, command in self._commands.iteritems():
                self._frozen_commands[command_type] = command

            self._commands.clear()

            # Toggle enabled off
            self._enabled.clear()

            # Unlock commands
            self._commands_lock.release()

    def _set_state(self, new_state):
        self._state = new_state

        if self.state_change_callback is not None:
            self.state_change_callback(self._state)

    def put_command(self, command):
        # Lock commands
        self._commands_lock.acquire()

        command_type = type(command).__name__
        self._commands[command_type] = command
        if self.enabled:
            self._commands_available = True

        # Unlock commands
        self._commands_lock.release()

    def get_command(self):
        command = None

        # Lock commands
        self._commands_lock.acquire()

        if len(self._frozen_commands) > 0:
            # If we have frozen commands available, take those
            command = self._select_command(self._frozen_commands)
        elif len(self._commands) > 0:
            # Otherwise, take from main commands
            command = self._select_command(self._commands)

        # Unlock commands
        self._commands_lock.release()

        if command == None:
            raise ValueError('No commands available.')

        return command

    def _select_command(self, command_dict):
        """ Helper to select commands from dictionary
        based on priority and timestamp.

        NOTE: Not thread safe.
        """
        # Available command types sorted by (priority, timestamp)
        by_priorities = sorted(command_dict.keys(), key = lambda x: \
            (-command_dict[x].priority, command_dict[x].timestamp))

        # Get oldest, highest priority command
        command = command_dict[by_priorities[0]]
        del command_dict[by_priorities[0]]

        # If that was the last available command, clear event
        if len(command_dict) == 0:
            self._commands_available = False

        return command

    def send(self, packet):
        self._socket.send(packet)

    def receive(self):
        return self._socket.receive()

class StopAndWaitThread(threading.Thread):

    def __init__(self, controller):
        threading.Thread.__init__(self)
        self.controller = controller
        self.daemon = True

    def run(self):
        sequence_number = 0
        command_successful = True
        command = None
        timestamp = datetime.now()

        while True:
            # If last command was successful, get next one
            send_mode = 'resend'
            if self.controller._state == States.running and \
                command is None and self.controller._commands_available:

                command = self.controller.get_command()
                send_mode = 'send'
                # print total_seconds(datetime.now() - timestamp)
                timestamp = datetime.now()

            if self.controller._state == States.wait_syn:
                # Send syn
                syn_packet = SerialPacket(sequence_number=0, \
                    acknowledge=False, synchronize=True)
                self.controller.send(syn_packet)
                if LOG_COMM:
                    print "send syn"
            elif command is not None:
                # Send command as packet
                packet = SerialPacket(sequence_number=sequence_number, \
                    acknowledge=False, synchronize=False, \
                    nested_header=command.serialize_type(), \
                    data=command.serialize_data())
                self.controller.send(packet)

            # Log sending/resending
            if LOG_COMM and send_mode == 'send' and command is not None and command.log:
                print '%s  %d [%s]' % ("send  " if send_mode else \
                    "resend", sequence_number, str(command))

            # Monitor responses until matching seq no is found
            while True:
                response_packet = None
                try:
                    response_packet = self.controller.receive()

                    if response_packet.synchronize:
                        if response_packet.acknowledge:
                            # SYN-ACK: move to running state
                            self.controller._set_state(States.running)
                            command = None
                        else:
                            # SYN: reset seq. no. and send SYN-ACK
                            self.sequence_number = 0
                            syn_ack = SerialPacket(acknowledge=True, \
                                synchronize=True)
                            self.controller.send(syn_ack)
                            command = None
                            if LOG_COMM:
                                print "send syn-ack"
                        break
                    elif response_packet.acknowledge:
                        # ACK: if correct ack, update seq. no.
                        if response_packet.sequence_number != sequence_number:
                            if LOG_COMM and command is not None and command.log:
                                print "success"
                            if command is not None:
                                sequence_number = 1 - sequence_number
                            command = None
                            break
                    elif response_packet.nested_header == 0:
                        # print "heartbeat"
                        pass
                except TimeoutException as e:
                    if LOG_COMM and command is not None and command.log:
                        print "timeout"
                    break
                except DecodeException as e:
                    if LOG_COMM and command is not None and command.log:
                        print "corrupt"

def total_seconds(dt):
    """ Number of seconds in dt (delta time). """
    return (dt.microseconds + (dt.seconds + dt.days * 24 * 3600) * 10**6) / float(10**6)
