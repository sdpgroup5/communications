'''Serial packets'''

# ACKnowledgement
_ack_bit = 0x10
# SYNchronize peer. Will reset peer's sequence number to 0
_syn_bit = 0x08
# ENForce packet (unset will not update sequence number)
_enf_bit = 0x04

class SerialPacket(object):
    def __init__(self, sequence_number = 0, acknowledge = False, \
        synchronize = False, nested_header = 0, data = None):

        if sequence_number < 0 or 1 < sequence_number:
            raise ValueError("Sequence number must be in range [0,1]")

        if not (nested_header & 0x7) is nested_header:
            raise ValueError("Max 3 bits for nested header")

        # Flags (seq. no., ACK flag, SYNCHRONIZE flag, NESTED HEADER)
        self.sequence_number = sequence_number
        self.acknowledge = acknowledge
        self.synchronize = synchronize
        self.nested_header = nested_header

        seq_bit = sequence_number << 5
        ack_bit = _ack_bit if acknowledge else 0
        syn_bit = _syn_bit if synchronize else 0
        nst_bits = nested_header & 0x7


        # TODO CRCs
        crc = 0

        # Build header
        self.header = [seq_bit | ack_bit | syn_bit | nst_bits, crc]

        # Data
        self.data = data if data is not None else []
        
    @classmethod
    def from_payload(klass, payload):
        # TODO CRCs

        sequence_number = payload[0] >> 5
        acknowledge = (payload[0] & _ack_bit) != 0
        synchronize = (payload[0] & _syn_bit) != 0
        nested_header = payload[0] & 0x7
        return klass(sequence_number, acknowledge, synchronize, \
            nested_header, payload[2:])
