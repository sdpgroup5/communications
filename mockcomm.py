class MockComm(object):

    def __init__(self, enabled=False, logcomm=True):
        self._enabled = enabled
        self._logcomm  = logcomm

    @property
    def enabled(self):
        return self._enabled

    @enabled.setter
    def enabled(self, value):
        self._enabled = value == 1 or value == True

    def put_command(self, command):
        if self._logcomm:
            print command
