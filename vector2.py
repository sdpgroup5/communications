import math

class Vector2():
    def __init__(self, x, y):
        self.x = x
        self.y = y

    @classmethod
    def from_angle(cls, angle, length):
        return cls(math.cos(angle) * length, math.sin(angle) * length)

    def __add__(self, v):
        return Vector2(self.x + v.x, self.y + v.y)

    def __sub__(self, v):
        return Vector2(self.x - v.x, self.y - v.y)

    def __mul__(self, s):
        return self.mul(s)

    def __div__(self, s):
        return self.div(s)

    def __truediv__(self, s):
        return self.div(s)

    def mul(self, s):
        return Vector2(self.x * s, self.y * s)

    def div(self, s):
        return Vector2(self.x / float(s), self.y / float(s))

    def truediv(self, s):
        return Vector2(self.x / float(s), self.y / float(s))

    def dot(self, v):
        """ Scalar product

        Args:
            (self)
            v: Vector2

        Returns:
            Dot product
        """
        return self.x * v.x + self.y * v.y

    def len(self):
        """ Length of vector """
        return math.sqrt(self.len2())

    def len2(self):
        """ Squared length of vector """
        return self.x * self.x + self.y * self.y

    def norm(self):
        """ Normalize. Returns this vector with unit length """
        try:
            return self * (1.0 / self.len())
        except ZeroDivisionError:
            return Vector2(0, 0)

    def angle_wrt(self, v):
        """ Angle relative to another vector v [0, 2pi]

        Args:
            (self)
            v: Vector2

        Returns:
            Angle in radians
        """
        return (math.atan2(self.y, self.x) - math.atan2(v.y, v.x)) % (math.pi * 2)

    def angle(self):
        """ Angle relative to x axis [0, 2pi]

        Args:
            (self)

        Returns:
            Angle in radians
        """
        return math.atan2(self.y, self.x) % (math.pi * 2)

    def rotate(self, angle):
        cos = math.cos(angle)
        sin = math.sin(angle)
        return Vector2(cos*self.x - sin*self.y, sin*self.x + cos*self.y)

    def rotate_pivot(self, pivot_point, angle):
      return (self - pivot_point).rotate(angle) + pivot_point

    def __repr__(self):
        return "(%+0.3f, %+0.3f)" % (self.x, self.y)

x_axis = Vector2(1, 0)
y_axis = Vector2(0, 1)
